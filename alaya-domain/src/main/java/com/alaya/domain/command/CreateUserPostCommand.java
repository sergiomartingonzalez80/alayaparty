package com.alaya.domain.command;

import com.alaya.ddd.commons.DomainCommand;
import com.alaya.domain.model.UserId;

public class CreateUserPostCommand extends DomainCommand {
    private final String email;
    private final String filename;
    private final byte[] image;
    private final UserId userId;


    public CreateUserPostCommand(UserId userId, String email, String filename, byte[] image) {
        this.filename = filename;
        this.image = image;
        this.userId = userId;
        this.email = email;
    }

    public UserId getUserId() {
        return userId;
    }

    public String getEmail() {
        return email;
    }

    public String getFilename() {
        return filename;
    }

    public byte[] getImage() {
        return image;
    }
}

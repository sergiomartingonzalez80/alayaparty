package com.alaya.domain.command;

import com.alaya.ddd.commons.DomainCommand;

import java.util.Set;

public class CreateUserCommand extends DomainCommand {
    private final String username;

    private final String email;

    private final Set<String> role;

    private final String password;

    public CreateUserCommand(String username, String email, Set<String> role, String password) {
        this.username = username;
        this.email = email;
        this.role = role;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public String getEmail() {
        return email;
    }

    public Set<String> getRole() {
        return role;
    }

    public String getPassword() {
        return password;
    }
}

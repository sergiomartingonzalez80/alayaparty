package com.alaya.domain.query;

import com.alaya.ddd.commons.DomainQuery;
import com.alaya.domain.model.PostId;
import com.alaya.domain.model.UserId;

public class SearchUserPostQuery extends DomainQuery {

    private final UserId userId;
    private final PostId postId;

    public SearchUserPostQuery(UserId userId, PostId postId) {
        this.userId = userId;
        this.postId = postId;
    }

    public UserId getUserId() {
        return userId;
    }

    public PostId getPostId() {
        return postId;
    }
}

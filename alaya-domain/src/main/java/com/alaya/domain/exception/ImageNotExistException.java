package com.alaya.domain.exception;

public class ImageNotExistException extends AlayaException {

    public ImageNotExistException(String error) {
        super(error);
    }
}

package com.alaya.domain.exception;

public class UserAlreadyExistException extends AlayaException {


    public UserAlreadyExistException(String error) {
        super(error);
    }
}

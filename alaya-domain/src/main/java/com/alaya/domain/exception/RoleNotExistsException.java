package com.alaya.domain.exception;

public class RoleNotExistsException extends AlayaException {

    public RoleNotExistsException(String error) {
        super(error);
    }
}

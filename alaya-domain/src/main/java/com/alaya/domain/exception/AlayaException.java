package com.alaya.domain.exception;

public class AlayaException extends RuntimeException {

    private final String error;

    public AlayaException(String error) {
        this.error = error;
    }

    public String getError() {
        return error;
    }
}

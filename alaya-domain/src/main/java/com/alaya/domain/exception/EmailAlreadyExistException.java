package com.alaya.domain.exception;

public class EmailAlreadyExistException extends AlayaException {

    public EmailAlreadyExistException(String error) {
        super(error);
    }
}

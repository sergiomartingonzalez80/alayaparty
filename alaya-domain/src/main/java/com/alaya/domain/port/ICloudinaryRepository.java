package com.alaya.domain.port;

import com.alaya.domain.model.Post;
import org.springframework.core.io.ByteArrayResource;

import java.io.IOException;
import java.util.List;


public interface ICloudinaryRepository {
    String uploadFile(String id, byte[] image) throws IOException;

    List<String> uploadFiles(Post post) throws IOException;

    boolean deleteFile(Post post) throws IOException;

    ByteArrayResource downloadImg(String publicId) throws IOException;

}

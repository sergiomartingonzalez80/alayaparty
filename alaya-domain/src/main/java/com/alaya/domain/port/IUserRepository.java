package com.alaya.domain.port;

import com.alaya.domain.model.ERole;
import com.alaya.domain.model.Role;
import com.alaya.domain.model.User;

import java.util.List;
import java.util.Optional;

public interface IUserRepository {

    User createOrSave(User user);

    Optional<User> findByUsername(String username);

    Boolean existsByUsername(String username);

    Boolean existsByEmail(String email);

    Optional<Role> findByName(ERole name);

    void saveAll(List<Role> roles);

    List<Role> findAll();

    User findById(String userId);
}

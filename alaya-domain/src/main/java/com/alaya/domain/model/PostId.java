package com.alaya.domain.model;

import com.alaya.ddd.commons.ValueObject;

public class PostId extends ValueObject {

    private final String postId;

    public PostId(String postId) {
        this.postId = postId;
    }

    public static PostId of(String postId) {
        return new PostId(postId);
    }

    public String value() {
        return postId;
    }
}

package com.alaya.domain.model;

import com.alaya.ddd.commons.Entity;

import java.util.List;
import java.util.Objects;
import java.util.UUID;

public class Post extends Entity {
    private final PostId postId;
    private final String description;
    private final String topic;
    private final String filename;
    private final List<byte[]> images;

    public Post(PostId postId, String description, String topic, String filename, List<byte[]> images) {
        this.postId = postId;
        this.description = description;
        this.topic = topic;
        this.filename = filename;
        this.images = images;
    }

    public PostId getPostId() {
        return postId;
    }

    public String getDescription() {
        return description;
    }

    public String getFilename() {
        return filename;
    }

    public List<byte[]> getImages() {
        return images;
    }

    public String getTopic() {
        return topic;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Post post = (Post) o;
        return Objects.equals(postId, post.postId) && Objects.equals(description, post.description) && Objects.equals(topic, post.topic) && Objects.equals(images, post.images);
    }

    @Override
    public int hashCode() {
        return Objects.hash(postId, description, topic, images);
    }

    public static final class PostBuilder {
        private PostId postId;
        private String description;
        private List<byte[]> images;

        private String topic;

        private String filename;

        private PostBuilder() {
        }

        public static PostBuilder aPost() {
            return new PostBuilder();
        }

        public PostBuilder withPostId(PostId postId) {
            this.postId = postId;
            return this;
        }

        public PostBuilder withDescription(String description) {
            this.description = description;
            return this;
        }

        public PostBuilder withImages(List<byte[]> images) {
            this.images = images;
            return this;
        }

        public PostBuilder withTopic(String topic) {
            this.topic = topic;
            return this;
        }

        public PostBuilder withFileName(String filename) {
            this.filename = filename;
            return this;
        }

        public Post instanciateExisting() {
            return new Post(postId, description, topic, filename, images);
        }

        public Post createNew() {
            return new Post(PostId.of(UUID.randomUUID().toString()), description, topic, filename, images);
        }
    }
}

package com.alaya.domain.model;

import com.alaya.ddd.commons.Entity;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

public class User extends Entity {

    private final UserId userId;

    private final String name;

    private final String email;

    private final String password;

    private final List<Role> roles;

    private List<Post> postList;


    public User(UserId userId, String name, String email, String password, List<Role> roles, List<Post> postList) {
        this.userId = userId;
        this.name = name;
        this.email = email;
        this.password = password;
        this.roles = roles;
        this.postList = postList;
    }

    public UserId getUserId() {
        return userId;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public List<Post> getPostList() {
        return postList;
    }


    public List<Role> getRoles() {
        return roles;
    }

    /**
     * Allowa to add a new post to user
     *
     * @param post
     */
    public void addPost(Post post) {
        ensurePostList();
        //Generate the event if would be necessary
        this.postList.add(post);
    }

    private void ensurePostList() {
        if (this.postList == null) this.postList = new ArrayList<>();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(userId, user.userId) && Objects.equals(name, user.name) && Objects.equals(email, user.email) && Objects.equals(password, user.password) && Objects.equals(roles, user.roles) && Objects.equals(postList, user.postList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, name, email, password, roles, postList);
    }

    public static final class UserBuilder {
        private UserId userId;
        private String name;
        private String email;
        private String password;
        private List<Post> postList;

        private List<Role> roles;

        private UserBuilder() {
        }

        public static UserBuilder anUser() {
            return new UserBuilder();
        }

        public UserBuilder withUserId(UserId userId) {
            this.userId = userId;
            return this;
        }

        public UserBuilder withName(String name) {
            this.name = name;
            return this;
        }

        public UserBuilder withEmail(String email) {
            this.email = email;
            return this;
        }

        public UserBuilder withPassword(String password) {
            this.password = password;
            return this;
        }

        public UserBuilder withPostList(List<Post> postList) {
            this.postList = postList;
            return this;
        }

        public UserBuilder withRoles(List<Role> roles) {
            this.roles = roles;
            return this;
        }

        public User instanciateExisting() {
            return new User(userId, name, email, password, roles, postList);
        }

        public User createNew() {
            return new User(UserId.of(UUID.randomUUID().toString()), name, email, password, roles, postList);
        }

    }
}

package com.alaya.domain.model;

import com.alaya.ddd.commons.ValueObject;

public class UserId extends ValueObject {

    private final String userId;

    public UserId(String userId) {
        this.userId = userId;
    }

    public static UserId of(String userId) {
        return new UserId(userId);
    }

    public String value() {
        return userId;
    }

}

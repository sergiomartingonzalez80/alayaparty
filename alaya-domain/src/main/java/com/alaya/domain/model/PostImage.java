package com.alaya.domain.model;

import java.util.Arrays;
import java.util.Objects;
import java.util.UUID;

public class PostImage {
    private final String imageId;
    private final String filename;
    private final byte[] image;

    public PostImage(String imageId, String filename, byte[] image) {
        this.imageId = imageId;
        this.filename = filename;
        this.image = image;

    }

    public String getFilename() {
        return filename;
    }

    public byte[] getImage() {
        return image;
    }

    public String getImageId() {
        return imageId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PostImage postImage = (PostImage) o;
        return Objects.equals(imageId, postImage.imageId) && Objects.equals(filename, postImage.filename) && Arrays.equals(image, postImage.image);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(imageId, filename);
        result = 31 * result + Arrays.hashCode(image);
        return result;
    }

    public static final class PostImageBuilder {
        private String imageId;
        private String filename;
        private byte[] image;

        private PostImageBuilder() {
        }
        public static PostImageBuilder aPostImage() {
            return new PostImageBuilder();
        }

        public PostImageBuilder withImageId(String imageId) {
            this.imageId = imageId;
            return this;
        }
        public PostImageBuilder withFilename(String filename) {
            this.filename = filename;
            return this;
        }
        public PostImageBuilder withImage(byte[] image) {
            this.image = image;
            return this;
        }
        public PostImage instanciateExisting() {
            return new PostImage(imageId, filename, image);
        }

        public PostImage createNew() {
            return new PostImage(UUID.randomUUID().toString(), filename, image);
        }
    }
}

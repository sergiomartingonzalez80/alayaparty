package com.alaya.jpa.repository;

import com.alaya.domain.model.ERole;
import com.alaya.domain.model.Role;
import com.alaya.domain.model.User;
import com.alaya.domain.port.IUserRepository;
import com.alaya.jpa.helper.RoleRepositoryHelper;
import com.alaya.jpa.helper.UserRepositoryHelper;
import com.alaya.jpa.mapper.RoleMapper;
import com.alaya.jpa.mapper.UserMapper;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
public class UserRepository implements IUserRepository {

    private final UserRepositoryHelper userRepositoryHelper;

    private final RoleRepositoryHelper roleRepositoryHelper;

    private final UserMapper userMapper;

    private final RoleMapper roleMapper;

    @Autowired
    public UserRepository(UserRepositoryHelper userRepositoryHelper, RoleRepositoryHelper roleRepositoryHelper, UserMapper userMapper, RoleMapper roleMapper) {
        this.userRepositoryHelper = userRepositoryHelper;
        this.roleRepositoryHelper = roleRepositoryHelper;
        this.userMapper = userMapper;
        this.roleMapper = roleMapper;
    }

    @Transactional
    @Override
    public User createOrSave(User user) {
        return userMapper.toDomain(userRepositoryHelper.save(userMapper.toJpa(user)));
    }

    @Override
    public Optional<User> findByUsername(String username) {
        return Optional.ofNullable(userMapper.toDomain(
                Objects.requireNonNull(userRepositoryHelper.findByUsername(username).orElse(null))));
    }

    @Override
    public Boolean existsByUsername(String username) {
        return userRepositoryHelper.existsByUsername(username);
    }

    @Override
    public Boolean existsByEmail(String email) {
        return userRepositoryHelper.existsByEmail(email);
    }

    @Override
    public Optional<Role> findByName(ERole name) {
        return Optional.ofNullable(roleMapper.toDomain(
                Objects.requireNonNull(roleRepositoryHelper.findByName(name).orElse(null)))
        );
    }

    @Override
    public void saveAll(List<Role> roles) {
        roleRepositoryHelper.saveAll(roles.stream().map(roleMapper::toJpa).collect(Collectors.toSet()));
    }

    @Override
    public List<Role> findAll() {
        return roleRepositoryHelper.findAll().stream().map(roleJpa -> new Role(roleJpa.getId(), roleJpa.getName())).collect(Collectors.toList());
    }

    @Override
    public User findById(String userId) {
        return userMapper.toDomain(Objects.requireNonNull(userRepositoryHelper.findById(userId).orElse(null)));
    }
}

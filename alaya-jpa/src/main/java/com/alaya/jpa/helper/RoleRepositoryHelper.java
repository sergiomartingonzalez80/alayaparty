package com.alaya.jpa.helper;

import com.alaya.domain.model.ERole;
import com.alaya.jpa.models.RoleJpa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepositoryHelper extends JpaRepository<RoleJpa, Long> {
    Optional<RoleJpa> findByName(ERole name);

}

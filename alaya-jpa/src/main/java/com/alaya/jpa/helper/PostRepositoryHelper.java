package com.alaya.jpa.helper;

import com.alaya.jpa.models.PostJpa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PostRepositoryHelper extends JpaRepository<PostJpa, Long> {


}

package com.alaya.jpa.helper;


import com.alaya.jpa.models.UserJpa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public interface UserRepositoryHelper extends JpaRepository<UserJpa, String> {
    Optional<UserJpa> findByUsername(String username);

    Boolean existsByUsername(String username);

    Boolean existsByEmail(String email);
}

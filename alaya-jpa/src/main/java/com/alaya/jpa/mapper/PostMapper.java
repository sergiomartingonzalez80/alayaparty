package com.alaya.jpa.mapper;

import com.alaya.domain.model.Post;
import com.alaya.domain.model.PostId;
import com.alaya.jpa.models.PostJpa;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class PostMapper {

    public Post toDomain(PostJpa postJpa) {
        return Post.PostBuilder.aPost()
                .withDescription(postJpa.getDescription())
                .withFileName(postJpa.getFileName())
                .withTopic(postJpa.getTopic())
                .withImages(List.of(postJpa.getData()))
                .withPostId(PostId.of(postJpa.getId()))
                .instanciateExisting();

    }

    public PostJpa toJpa(Post post) {
        PostJpa postJpa = new PostJpa();
        postJpa.setData(post.getImages().get(0));
        postJpa.setId(post.getPostId().value());
        postJpa.setFileName(post.getFilename());
        postJpa.setDescription(post.getDescription());
        postJpa.setTopic(post.getTopic());
        //TODO diff fileType diff post just for the future
        postJpa.setFileType(null);
        return postJpa;

    }
}

package com.alaya.jpa.mapper;


import com.alaya.domain.model.User;
import com.alaya.domain.model.UserId;
import com.alaya.jpa.models.UserJpa;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class UserMapper {

    private final RoleMapper roleMapper;

    private final PostMapper postMapper;

    @Autowired
    public UserMapper(RoleMapper roleMapper, PostMapper postMapper) {
        this.roleMapper = roleMapper;
        this.postMapper = postMapper;
    }

    public User toDomain(UserJpa userJpa) {
        if (Objects.isNull(userJpa)) {
            return null;
        }
        return User.UserBuilder.anUser()
                .withEmail(userJpa.getEmail())
                .withPassword(userJpa.getPassword())
                .withRoles(userJpa.getRoles().stream().map(roleMapper::toDomain).collect(Collectors.toList()))
                .withPostList(userJpa.getPosts().stream().map(postMapper::toDomain).collect(Collectors.toList()))
                .withName(userJpa.getUsername()).withUserId(UserId.of(userJpa.getId()))
                .instanciateExisting();

    }

    public UserJpa toJpa(User user) {
        UserJpa userJpa = new UserJpa();
        userJpa.setId(user.getUserId().value());
        userJpa.setPassword(user.getPassword());
        userJpa.setEmail(user.getEmail());
        userJpa.setUsername(user.getName());
        userJpa.setRoles(user.getRoles().stream().map(roleMapper::toJpa).collect(Collectors.toSet()));
        userJpa.setPosts(user.getPostList().stream().map(postMapper::toJpa).collect(Collectors.toSet()));
        return userJpa;

    }
}

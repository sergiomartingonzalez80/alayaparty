package com.alaya.jpa.mapper;


import com.alaya.domain.model.Role;
import com.alaya.jpa.models.RoleJpa;
import org.springframework.stereotype.Component;

@Component
public class RoleMapper {

    public Role toDomain(RoleJpa roleJpa) {
        return new Role(roleJpa.getId(), roleJpa.getName());
    }

    public RoleJpa toJpa(Role role) {
        return new RoleJpa(role.getId(), role.getName());
    }
}

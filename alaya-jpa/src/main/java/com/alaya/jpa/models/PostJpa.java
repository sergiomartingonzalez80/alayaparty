package com.alaya.jpa.models;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Lob;
import jakarta.persistence.Table;

import java.util.Arrays;
import java.util.Objects;

@Entity
@Table(name = "posts")
public class PostJpa {

    @Id
    private String id;

    private String fileName;

    private String fileType;

    private String description;

    private String topic;

    @Lob
    private byte[] data;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PostJpa postJpa = (PostJpa) o;
        return Objects.equals(id, postJpa.id) && Objects.equals(fileName, postJpa.fileName) && Objects.equals(fileType, postJpa.fileType) && Objects.equals(description, postJpa.description) && Objects.equals(topic, postJpa.topic) && Arrays.equals(data, postJpa.data);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(id, fileName, fileType, description, topic);
        result = 31 * result + Arrays.hashCode(data);
        return result;
    }
}

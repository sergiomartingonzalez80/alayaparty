package com.alaya.cloudinary.service;

import com.alaya.domain.model.Post;
import com.alaya.domain.port.ICloudinaryRepository;
import com.cloudinary.Cloudinary;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@Service
public class CloudinaryService implements ICloudinaryRepository {


    private final Cloudinary cloudinary;

    @Autowired
    public CloudinaryService(Cloudinary cloudinary) {
        this.cloudinary = cloudinary;
    }

    @Override
    public String uploadFile(String id, byte[] image) throws IOException {
        try {
            return cloudinary.uploader().upload(image, Map.of("public_id", id)).get("url").toString();
        } catch (IOException e) {
            //TODO add proper logger
            throw e;

        }
    }

    @Override
    public List<String> uploadFiles(Post post) {
        List<String> urlImages = new ArrayList<>();
        if (!CollectionUtils.isEmpty(post.getImages())) {
            post.getImages().stream().forEach(bytes -> {
                try {
                    //TODO not sure if be can upload diff pictures same id
                    urlImages.add(uploadFile(post.getPostId().value(), bytes));
                } catch (IOException ex) {
                    throw new RuntimeException(ex);
                }
            });
        }
        return urlImages;
    }

    @Override
    public boolean deleteFile(Post post) throws IOException {
        try {
            cloudinary.uploader().destroy(post.getPostId().value(), Map.of("public_id", post.getPostId().value()));
        } catch (IOException e) {
            //TODO add proper logger
            throw e;

        }
        return true;
    }

    @Override
    public ByteArrayResource downloadImg(String publicId) throws IOException {
        String cloudUrl = cloudinary.url().secure(true)
                //TODO reminder we can tune image
                //.format(format)
                //.transformation(transformation)
                .publicId(publicId)
                .generate();
        try {
            // Get a ByteArrayResource from the URL
            URL url = new URL(cloudUrl);
            InputStream inputStream = url.openStream();
            byte[] out = IOUtils.toByteArray(inputStream);
            return new ByteArrayResource(out);


        } catch (IOException ex) {
            throw ex;
        }


    }

}

package com.alaya.cloudinary.config;

import com.cloudinary.Cloudinary;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class CloudinaryConfiguration {
    /*
    * import com.cloudinary.*;
...
Cloudinary cloudinary = new Cloudinary(ObjectUtils.asMap(
  "cloud_name", "dyhwnrjjn",
  "api_key", "857741459534289",
  "api_secret", "6U09Hc25mqy62mxBTfxcIOCocJE"));*/
    private static final String CLOUD_NAME = "dyhwnrjjn";
    private static final String API_KEY = "857741459534289";
    private static final String API_SECRET = "6U09Hc25mqy62mxBTfxcIOCocJE";

    private static final String PUBLIC_ID = "public_id";

    @Bean
    public Cloudinary cloudinary() {
        Map<String, String> config = new HashMap<>();
        config.put("cloud_name", CLOUD_NAME);
        config.put("api_key", API_KEY);
        config.put("api_secret", API_SECRET);
        return new Cloudinary(config);
    }
}

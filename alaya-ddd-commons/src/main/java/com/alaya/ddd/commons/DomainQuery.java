package com.alaya.ddd.commons;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.UUID;

/**
 * Contains properties to identify a query
 */
public abstract class DomainQuery {
    protected final String queryUid;
    protected final ZonedDateTime occurredOn;


    public DomainQuery() {
        occurredOn = ZonedDateTime.now(ZoneOffset.UTC);
        queryUid = UUID.randomUUID().toString();
    }

    public String commandUid() {
        return queryUid;
    }

    public ZonedDateTime occurredOn() {
        return occurredOn;
    }

    public String commandName() {
        return this.getClass().getName();
    }

}

package com.alaya.ddd.commons;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

public interface QueryHandler<T extends DomainQuery> {

    Data<T> handle(T command);

    default Class<T> handles() {
        Class clazz = getClass();
        ParameterizedType parameterizedType = (ParameterizedType) clazz.getGenericInterfaces()[0];
        Type[] typeArguments = parameterizedType.getActualTypeArguments();
        return (Class<T>) typeArguments[0];
    }
}

package com.alaya.ddd.commons;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.UUID;

/**
 * Contains properties to identify a command
 */
public abstract class DomainCommand {
    protected final String commandUid;
    protected final ZonedDateTime occurredOn;


    public DomainCommand() {
        occurredOn = ZonedDateTime.now(ZoneOffset.UTC);
        commandUid = UUID.randomUUID().toString();
    }

    public String commandUid() {
        return commandUid;
    }

    public ZonedDateTime occurredOn() {
        return occurredOn;
    }

    public String commandName() {
        return this.getClass().getName();
    }

}

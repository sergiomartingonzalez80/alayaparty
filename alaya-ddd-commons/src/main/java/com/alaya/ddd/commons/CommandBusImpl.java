package com.alaya.ddd.commons;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Synchronous impl to commandBus pattern
 */
@Named
public class CommandBusImpl implements CommandBus {

    private final Map<String, CommandHandler> handlersMap = new HashMap<>();

    @Inject
    public CommandBusImpl(List<CommandHandler<? extends DomainCommand>> handlersList) {
        if (Objects.nonNull(handlersList)) {
            handlersList.forEach(this::registerHandler);
        }
    }

    @Override
    public void registerHandler(CommandHandler<? extends DomainCommand> handler) {
        handlersMap.put(handler.handles().getName(), handler);
    }

    @Override
    public void push(DomainCommand command) throws IOException {
        if (!handlersExistsFor(command.getClass().getName())) {
            throw new IllegalStateException("Command handler not found for [" + command.getClass().getName() + "]");
        }
        handlersMap.get(command.getClass().getName()).handle(command);
    }

    @Override
    public void pushAndForget(DomainCommand command) throws IOException {
        if (handlersExistsFor(command.getClass().getName())) {
            handlersMap.get(command.getClass().getName()).handle(command);
        }
    }

    private boolean handlersExistsFor(String handles) {
        return this.handlersMap.containsKey(handles) && this.handlersMap.get(handles) != null;
    }
}

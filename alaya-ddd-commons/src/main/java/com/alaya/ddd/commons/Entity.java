package com.alaya.ddd.commons;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public abstract class Entity {
    @NotNull
    @Valid
    private final List<@NotNull DomainEvent> events;
    @PositiveOrZero
    private long version;

    public Entity() {
        this.events = new ArrayList<>();
    }

    public long getVersion() {
        return version;
    }

    protected void setVersion(long version) {
        this.version = version;
    }

    protected void publishEvent(DomainEvent event) {
        this.events.add(event);
    }

    public boolean hasEvents() {
        return !events.isEmpty();
    }

    /**
     * Retrieve the events
     *
     * @return
     */
    public List<DomainEvent> getDomainEvents() {
        return Collections.unmodifiableList(events);
    }
}

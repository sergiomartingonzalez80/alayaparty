package com.alaya.ddd.commons;

public interface QueryBus {

    void registerHandler(final QueryHandler<? extends DomainQuery> handler);

    Data query(final DomainQuery query);
}

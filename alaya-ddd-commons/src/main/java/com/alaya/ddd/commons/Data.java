package com.alaya.ddd.commons;

public class Data<T> {
    private T t;

    public void set(T t) {
        this.t = t;
    }

    public T get() {
        return t;
    }

}

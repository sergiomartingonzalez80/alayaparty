package com.alaya.ddd.commons;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Synchronous impl to make easier CQRS pattern
 */
@Named
public class QueryBusImpl implements QueryBus {

    private final Map<String, QueryHandler> handlersMap = new HashMap<>();

    @Inject
    public QueryBusImpl(List<QueryHandler<? extends DomainQuery>> handlersList) {
        if (Objects.nonNull(handlersList)) {
            handlersList.forEach(this::registerHandler);
        }
    }

    @Override
    public void registerHandler(QueryHandler<? extends DomainQuery> handler) {
        handlersMap.put(handler.handles().getName(), handler);
    }

    @Override
    public Data query(DomainQuery query) {
        if (!handlersExistsFor(query.getClass().getName())) {
            throw new IllegalStateException("Query handler not found for [" + query.getClass().getName() + "]");
        }
        return handlersMap.get(query.getClass().getName()).handle(query);
    }

    private boolean handlersExistsFor(String handles) {
        return this.handlersMap.containsKey(handles) && this.handlersMap.get(handles) != null;
    }


}

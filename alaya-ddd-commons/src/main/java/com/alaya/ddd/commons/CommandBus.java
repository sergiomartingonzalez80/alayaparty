package com.alaya.ddd.commons;

import java.io.IOException;

/**
 *
 */
public interface CommandBus {

    void registerHandler(final CommandHandler<? extends DomainCommand> handler);

    void push(final DomainCommand command) throws IOException;

    void pushAndForget(final DomainCommand command) throws IOException;

}

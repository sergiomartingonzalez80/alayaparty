package com.alaya.controller.mapper;

import com.alaya.controller.request.SignupRequest;
import com.alaya.domain.command.CreateUserCommand;

public class UserCreateRequestToDomainMapper {

    public static CreateUserCommand userRequestToDomainCommand(SignupRequest userCreateRequest) {
        //TODO pending transformation
        return new CreateUserCommand(userCreateRequest.getUsername(), userCreateRequest.getEmail(), userCreateRequest.getRole(), userCreateRequest.getPassword());
    }
}

package com.alaya.controller;

import com.alaya.controller.jwt.JwtUtils;
import com.alaya.controller.jwt.UserDetailsImpl;
import com.alaya.controller.mapper.UserCreateRequestToDomainMapper;
import com.alaya.controller.request.LoginRequest;
import com.alaya.controller.request.SignupRequest;
import com.alaya.controller.response.ImageResponse;
import com.alaya.controller.response.MessageResponse;
import com.alaya.controller.response.UserInfoResponse;
import com.alaya.ddd.commons.CommandBus;
import com.alaya.ddd.commons.Data;
import com.alaya.ddd.commons.QueryBus;
import com.alaya.domain.command.CreateUserCommand;
import com.alaya.domain.command.CreateUserPostCommand;
import com.alaya.domain.exception.AlayaException;
import com.alaya.domain.exception.EmailAlreadyExistException;
import com.alaya.domain.exception.RoleNotExistsException;
import com.alaya.domain.exception.UserAlreadyExistException;
import com.alaya.domain.model.PostId;
import com.alaya.domain.model.UserId;
import com.alaya.domain.query.SearchUserPostQuery;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseCookie;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("alaya/users")
@Tag(name = "user management", description = "Rest controller to create user and add post.")
public class UserController {
    protected static final String SUCCESS_MESSAGE = "User created successfully";

    private final CommandBus commandBus;

    private final QueryBus queryBus;

    private final JwtUtils jwtUtils;

    private final AuthenticationManager authenticationManager;

    @Autowired
    public UserController(CommandBus commandBus, QueryBus queryBus, JwtUtils jwtUtils, AuthenticationManager authenticationManager) {
        this.commandBus = commandBus;
        this.queryBus = queryBus;
        this.jwtUtils = jwtUtils;
        this.authenticationManager = authenticationManager;
    }


    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

        Authentication authentication = authenticationManager
                .authenticate(new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();

        ResponseCookie jwtCookie = jwtUtils.generateJwtCookie(userDetails);

        List<String> roles = userDetails.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .toList();
        return ResponseEntity.ok().header(HttpHeaders.SET_COOKIE, jwtCookie.toString())
                .body(new UserInfoResponse(userDetails.getId(),
                        userDetails.getUsername(),
                        userDetails.getEmail(),
                        roles,
                        jwtCookie.toString()));


    }

    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {
        CreateUserCommand userCommand = UserCreateRequestToDomainMapper.userRequestToDomainCommand(signUpRequest);
        try {
            commandBus.push(userCommand);
        } catch (RoleNotExistsException | UserAlreadyExistException | EmailAlreadyExistException e) {
            return ResponseEntity.badRequest().body(new MessageResponse(e.getError()));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
    }


    @PostMapping("/signout")
    public ResponseEntity<?> logoutUser() {
        ResponseCookie cookie = jwtUtils.getCleanJwtCookie();
        return ResponseEntity.ok().header(HttpHeaders.SET_COOKIE, cookie.toString())
                .body(new MessageResponse("You've been signed out!"));
    }

    @PostMapping(
            value = "/add",
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE}
    )
    @ResponseStatus(HttpStatus.OK)
    @Operation(summary = "create new user")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200", description = SUCCESS_MESSAGE
            )
    })
    public ResponseEntity<String> createUser(@Valid @RequestBody SignupRequest userCreateRequest) throws AlayaException, IOException {
        CreateUserCommand userCommand = UserCreateRequestToDomainMapper.userRequestToDomainCommand(userCreateRequest);
        commandBus.push(userCommand);
        return new ResponseEntity<>(SUCCESS_MESSAGE, HttpStatus.OK);
    }


    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    @PostMapping("/post/{userId}")
    public ResponseEntity<String> uploadImage(@PathVariable("userId") String userId,
                                              @RequestParam("image") MultipartFile file)
            throws IOException {
        CreateUserPostCommand command = new CreateUserPostCommand(UserId.of(userId),
                "",
                file.getOriginalFilename(),
                file.getBytes()
        );
        commandBus.push(command);

        return ResponseEntity.status(HttpStatus.OK)
                .body(file.getOriginalFilename());

    }


    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    @GetMapping(value = "/image/{userId}/{imageId}")
    @ResponseBody
    public ResponseEntity<ImageResponse> getImage(@PathVariable("userId") String userId, @PathVariable("imageId") String imageId) {
        SearchUserPostQuery query = new SearchUserPostQuery(UserId.of(userId), PostId.of(imageId));

        Data data = queryBus.query(query);
        ImageResponse image = new ImageResponse();
        image.setImage((byte[]) data.get());
        image.setFilename(imageId);
        return ResponseEntity.ok()
                // .contentType(MediaType.IMAGE_PNG)
                .body(image);

        //TODO to see image in browser data:image/jpg;base64,ENCODEDBYTEARRAY!

    }

}

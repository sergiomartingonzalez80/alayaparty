package com.alaya.controller.response;

import org.springframework.web.ErrorResponse;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class ErrorResponses {
    private final List<ErrorResponse> errors = new ArrayList();

    public ErrorResponses() {
    }

    public List<ErrorResponse> getErrors() {
        return Collections.unmodifiableList(this.errors);
    }

    public void addError(ErrorResponse errorResponse) {
        this.errors.add(errorResponse);
    }

    public List<String> details() {
        return (List) this.errors.stream().map(ErrorResponse::getBody).collect(Collectors.toList());
    }
}
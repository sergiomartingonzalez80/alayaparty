package com.alaya.controller;

import com.alaya.controller.response.ErrorResponses;
import com.alaya.domain.exception.AlayaException;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
public class WebRestControllerAdvice extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = {AuthenticationCredentialsNotFoundException.class})
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ApiResponse(
            responseCode = "401",
            description = "User not authenticated",
            content = @Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = @Schema(implementation = ErrorResponses.class)
            )
    )
    public ResponseEntity<String> handleAuthenticationCredentialsNotFoundException(AuthenticationCredentialsNotFoundException ex) {
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(ex.getMessage());
    }

    @ExceptionHandler(value = {AlayaException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ApiResponse(
            responseCode = "404",
            description = "",
            content = @Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = @Schema(implementation = ErrorResponses.class)
            )
    )
    public ResponseEntity<String> handleAlayaException(AlayaException ex) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ex.getError());
    }


//    @ExceptionHandler(value = {AuthenticationCredentialsNotFoundException.class, RoleNotExistsException.class})
//    @ResponseStatus(HttpStatus.NOT_FOUND)
//    @ApiResponse(
//            responseCode = "404",
//            description = "User not authenticated",
//            content = @Content(
//                    mediaType = MediaType.APPLICATION_JSON_VALUE,
//                    schema = @Schema(implementation = ErrorResponses.class)
//            )
//    )
//    public ResponseEntity<String> handleNotFoundElementRocheException(AuthenticationCredentialsNotFoundException ex) {
//        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(ex.getMessage());
//    }
//
//    @ExceptionHandler(value = {IllegalArgumentRocheException.class})
//    @ResponseStatus(HttpStatus.BAD_REQUEST  )
//    @ApiResponse(
//            responseCode = "400",
//            description = "Bad request (incomplete dictionary or entry already exists)",
//            content = @Content(
//                    mediaType = MediaType.APPLICATION_JSON_VALUE,
//                    schema = @Schema(implementation = ErrorResponses.class)
//            )
//    )
//    public ResponseEntity<String> handleIllegalArgumentRocheException(IllegalArgumentRocheException ex) {
//        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ex.getMessage());
//    }
//
//    @ExceptionHandler(value = {Exception.class})
//    @ResponseStatus(HttpStatus.BAD_REQUEST)
//    @ApiResponse(
//            responseCode = "400",
//            description = "Bad request (incomplete dictionary or entry already exists)",
//            content = @Content(
//                    mediaType = MediaType.APPLICATION_JSON_VALUE,
//                    schema = @Schema(implementation = ErrorResponses.class)
//            )
//    )
//    public ResponseEntity<String> handleException(Exception ex) {
//        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ex.getMessage());
//    }
//
//    @ExceptionHandler({
//            UnavailableApplicationRocheException.class,
//            UnexpectedStateRocheException.class
//    })
//    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
//    @ApiResponse(
//            responseCode = "500",
//            description = "Internal server error",
//            content = @Content(
//                    mediaType = MediaType.APPLICATION_JSON_VALUE,
//                    schema = @Schema(implementation = ErrorResponses.class)
//            )
//    )
//    public ResponseEntity<String> handleUnavailableApplicationRocheException(RocheException ex) {
//        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal server error, retry later");
//    }
//
//    @Override
//    public ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatusCode status, WebRequest request) {
//        String errorsDetails = ex.getBindingResult().getAllErrors().stream()
//                .map(error->((FieldError) error).getField())
//                .collect(Collectors.joining(", ", "{", "}"));
//        Map<String, Object> ids = Map.of(
//                "path", request.getContextPath(),
//                "errorDetails", errorsDetails
//                );
//        workflowLogger.log(Severity.ERROR, ServiceCodes.INVALID_SCHEMA,
//                WorkflowLogMessageDetails.builder().messagePayloadIds(ids).build(),
//                ex);
//
//        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ServiceCodes.INVALID_SCHEMA.getMessage());
//    }
//
//    @Override
//    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatusCode status, WebRequest request) {
//        Map<String, Object> ids = Map.of(
//                "path", request.getContextPath()
//        );
//        workflowLogger.log(Severity.ERROR, ServiceCodes.INVALID_SCHEMA,
//                WorkflowLogMessageDetails.builder().messagePayloadIds(ids).build(),
//                ex);
//
//        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ServiceCodes.INVALID_SCHEMA.getMessage());
//    }

}

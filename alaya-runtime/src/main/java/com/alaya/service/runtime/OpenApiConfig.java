package com.alaya.service.runtime;


import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OpenApiConfig {
    @Bean
    public OpenAPI openApi() {

        Contact contact = new Contact();
        contact.setName("alaya developers");
        contact.setEmail("developers@alaya.com");

        return new OpenAPI()
                .info(
                        new Info()
                                .title("Alaya API")
                                .version("1.0")
                                .description("Service to create user account & add/delete post to user)" +
                                        "<br>" +
                                        "Used charset for all messages is utf-8.")
                                .contact(contact)
                );
    }
}

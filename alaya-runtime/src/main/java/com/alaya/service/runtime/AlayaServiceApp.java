package com.alaya.service.runtime;

import com.alaya.domain.model.ERole;
import com.alaya.domain.model.Role;
import com.alaya.jpa.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.event.EventListener;

import java.util.List;


@SpringBootApplication
@ComponentScan(basePackages = {"com.alaya"})
public class AlayaServiceApp {
    //TODO just necessary the first time h2 is configured to be saved in file
    @Autowired
    UserRepository roleRepository;

    public static void main(String[] args) {
        SpringApplication.run(AlayaServiceApp.class, args);
    }

    @EventListener(ApplicationStartedEvent.class)
    public void createH2Data() {

        if (roleRepository.findAll().isEmpty()) {
            //Moderator not necessary just to play
            roleRepository.saveAll(List.of(new Role(1, ERole.ROLE_ADMIN), new Role(2, ERole.ROLE_USER), new Role(3, ERole.ROLE_MODERATOR)));
        }
    }
}
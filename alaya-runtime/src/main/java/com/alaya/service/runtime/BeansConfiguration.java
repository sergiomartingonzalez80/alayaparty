package com.alaya.service.runtime;

import com.alaya.application.command.handler.CreateUserCommandHandler;
import com.alaya.application.command.handler.CreateUserPostCommandHandler;
import com.alaya.application.query.handler.SearchUserPostQueryHandler;
import com.alaya.ddd.commons.CommandBus;
import com.alaya.ddd.commons.CommandBusImpl;
import com.alaya.ddd.commons.QueryBus;
import com.alaya.ddd.commons.QueryBusImpl;
import com.alaya.domain.port.ICloudinaryRepository;
import com.alaya.domain.port.IUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.List;

@EnableJpaRepositories(basePackages = {"com.alaya"})
@EntityScan(basePackages = {"com.alaya"})
@Configuration
public class BeansConfiguration {

    @Autowired
    IUserRepository userRepository;

    @Autowired
    ICloudinaryRepository cloudinaryRepository;

    @Autowired
    PasswordEncoder encoder;

    @Bean
    public CommandBus commandBus() {
        return new CommandBusImpl(List.of(new CreateUserCommandHandler(userRepository, encoder),
                new CreateUserPostCommandHandler(userRepository, cloudinaryRepository)));
    }

    @Bean
    public QueryBus queryBus() {
        return new QueryBusImpl(List.of(new SearchUserPostQueryHandler(userRepository)));
    }

}

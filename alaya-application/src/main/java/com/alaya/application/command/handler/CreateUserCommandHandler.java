package com.alaya.application.command.handler;

import com.alaya.ddd.commons.CommandHandler;
import com.alaya.domain.command.CreateUserCommand;
import com.alaya.domain.exception.EmailAlreadyExistException;
import com.alaya.domain.exception.RoleNotExistsException;
import com.alaya.domain.exception.UserAlreadyExistException;
import com.alaya.domain.model.ERole;
import com.alaya.domain.model.Role;
import com.alaya.domain.model.User;
import com.alaya.domain.port.IUserRepository;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Named
public class CreateUserCommandHandler implements CommandHandler<CreateUserCommand> {

    private final IUserRepository userRepository;

    private final PasswordEncoder encoder;

    @Inject
    public CreateUserCommandHandler(IUserRepository userRepository, PasswordEncoder encoder) {
        this.userRepository = userRepository;
        this.encoder = encoder;
    }

    @Override
    public void handle(CreateUserCommand command) {

        if (userRepository.existsByUsername(command.getUsername())) {
            throw new UserAlreadyExistException("User name already exists! ");
        }

        if (userRepository.existsByEmail(command.getEmail())) {
            throw new EmailAlreadyExistException("User email already exists! ");
        }

        //1.build the user
        User user = User.UserBuilder.anUser()
                .withName(command.getUsername())
                .withPassword(encoder.encode(command.getPassword()))
                .withEmail(command.getEmail())
                .withRoles(command.getRole().stream().map(
                                rol -> {
                                    try {
                                        Optional<Role> role = userRepository.findByName(ERole.valueOf(rol));
                                        if (Objects.isNull(role)) {
                                            throw new RoleNotExistsException("Role not exists! possible role ROLE_USER, ROLE_ADMIN");
                                        }
                                        return role.orElse(null);
                                    } catch (IllegalArgumentException i) {
                                        throw new RoleNotExistsException("Role not exists! possible role ROLE_USER, ROLE_ADMIN");
                                    }


                                }
                        )
                        .filter(Objects::nonNull)
                        .collect(Collectors.toList()))
                .createNew();

        userRepository.createOrSave(user);
    }
}

package com.alaya.application.command.handler;

import com.alaya.ddd.commons.CommandHandler;
import com.alaya.domain.command.CreateUserPostCommand;
import com.alaya.domain.model.Post;
import com.alaya.domain.model.User;
import com.alaya.domain.port.ICloudinaryRepository;
import com.alaya.domain.port.IUserRepository;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.util.List;

@Named
public class CreateUserPostCommandHandler implements CommandHandler<CreateUserPostCommand> {

    private final IUserRepository userRepository;
    private final ICloudinaryRepository cloudinaryRepository;

    @Inject
    public CreateUserPostCommandHandler(IUserRepository userRepository, ICloudinaryRepository cloudinaryRepository) {
        this.userRepository = userRepository;
        this.cloudinaryRepository = cloudinaryRepository;
    }

    @Override
    public void handle(CreateUserPostCommand command) throws IOException {

        User user = userRepository.findById(command.getUserId().value());

        Post newPost = Post.PostBuilder.aPost()
                .withImages(List.of(command.getImage()))
                .withTopic("topic")
                .withFileName(command.getFilename())
                .withDescription("description")
                .createNew();

        user.addPost(newPost);

        userRepository.createOrSave(user);
        Post post = user.getPostList().get(0);

        cloudinaryRepository.uploadFile(post.getPostId().value(), post.getImages().get(0));
    }
}

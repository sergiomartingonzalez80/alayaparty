package com.alaya.application.query.handler;

import com.alaya.ddd.commons.Data;
import com.alaya.ddd.commons.QueryHandler;
import com.alaya.domain.exception.ImageNotExistException;
import com.alaya.domain.model.Post;
import com.alaya.domain.model.User;
import com.alaya.domain.port.IUserRepository;
import com.alaya.domain.query.SearchUserPostQuery;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Objects;

@Named
public class SearchUserPostQueryHandler implements QueryHandler<SearchUserPostQuery> {

    private final IUserRepository userRepository;

    @Inject
    public SearchUserPostQueryHandler(IUserRepository userRepository) {
        this.userRepository = userRepository;
    }


    @Override
    public Data handle(SearchUserPostQuery query) {
        User user = userRepository.findById(query.getUserId().value());
        Data data = new Data();
        Post postUser = user.getPostList().stream()
                .filter(post -> post.getPostId().value().equals(query.getPostId().value()))
                .findFirst().orElse(null);
        if (Objects.isNull(postUser)) {
            throw new ImageNotExistException("Image not exists!!! ");
        }
        data.set(postUser.getImages().get(0));
        return data;
    }
}

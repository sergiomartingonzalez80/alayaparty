import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class StepDefs extends SpringIntegrationTest {
    @When("the alaya service receives a new user")
    public void theAlayaServiceReceivesANewUser() {

    }

    @Then("the alaya service should reply {int} http code and {string} body")
    public void theAlayaServiceShouldReplyHttpCodeAndBody(int arg0, String arg1) {
    }

    @And("the new user signin")
    public void theNewUserSignin() {
    }

    @And("the alaya service should log {string} and the user receives proper token")
    public void theAlayaServiceShouldLogAndTheUserReceivesProperToken(String arg0) {
    }
}

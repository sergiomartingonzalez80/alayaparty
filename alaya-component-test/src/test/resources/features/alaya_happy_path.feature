Feature: Result Service component tests

  Narrative:
  Happy paths from result service order results & report stored and retrieved properly

  Scenario: alaya_service_add_user_and_signin
    When the alaya service receives a new user
    Then the alaya service should reply 200 http code and "user created successfully" body
    And the new user signin
    And the alaya service should log "XXXXX" and the user receives proper token
